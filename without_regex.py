data = """
hello 000200-2222222
zello 010200-1234444
ZZZZ 111111-2222222
"""

parse = []
for line in data.split("\n"):
    print("---checking for [{}]---".format(line))
    for string in line.split(" "):
        if len(string) == 14:
            string = string[:6] + "-*******"
            parse.append(string)
    print("---checked---")
    print('\n')
print('\n'.join(parse))


import re
pat = re.compile("(\d{6})[-]\d{7}")
print(pat.sub("\g<1>-********", data))


