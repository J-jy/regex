import re

p = re.compile('[a-z]+')
m = p.match("3 python")
m1 = p.match('python')
s = p.search("3 python")
findall = p.findall("3 p2y5thon")
print("\n".join(findall))
print(m)
print(m1)
print(s)
print()
result = p.findall("life is tooooo short, arn't you")
print(">> ", end='')
print("\n>> ".join(result))

print('\n'*5)
print("------finditer TEST-------")
iter_result = p.finditer("life is tooooo short, arn't you")
for r in iter_result:
    # print(r)    # each object is 'match' type
    print(r.group())
    # print(r.start(), r.end())
    # print(r.span())
    HELPER = "r.span() == (r.start(), r.end())"
    if r.span() == (r.start(), r.end()):
        print(HELPER)
    else:
        print("NOT", HELPER)
# Let's use match object


p = re.compile("[a-z]+")
m = p.match("python")
# is same as
m1 = re.match("[a-z]+", 'python')

if m.group() == m1.group():
    print("checking, re.match Not None")
